#!/bin/bash
# Backs up the /var/lib/jenkins directory and uploads the archive to artifactory

TIMESTAMP="$(date +'%Y%m%d-%H%M%S')"
logger JENKINSBACKUP: Starting Jenkins backup at $TIMESTAMP
ARCHIVENAME=jenkins-config-$TIMESTAMP.tar.gz
UPLOADREPO="http://3.126.195.89:8081/artifactory/backup/jenkins-backups/"
BACKUPFOLDER=/var/lib/jenkins

tar -zcf jenkins-config-$TIMESTAMP.tar.gz $BACKUPFOLDER
logger JENKINSBACKUP: Wrote archive $ARCHIVENAME

logger JENKINSBACKUP: Uploading $ARCHIVENAME to $UPLOADREPO
curl -ubobojon:APAjB9LYqJSFazCVEQyfk6ZKZCi -T $ARCHIVENAME  $UPLOADREPO/$ARCHIVENAME

logger JENKINSBACKUP: Deleting archive file $ARCHIVENAME
rm -f $ARCHIVENAME

logger JENKINSBACKUP: Backup complete!
